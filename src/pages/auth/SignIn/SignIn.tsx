import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useNavigate } from 'react-router';
import { useAppSelector } from 'hooks';
import { isAuthenticated } from 'lib/contexts/auth';
import * as C from 'components/contexts';
import * as CS from 'components/structure';
import { ROUTES } from 'constants/urls';
import * as S from './SignIn.styles';

const SignIn = () => {
  const { signed } = useAppSelector((state) => state.auth);
  const navigate = useNavigate();

  useEffect(() => {
    if (isAuthenticated()) navigate(ROUTES.app.getLink('books'));
  }, [navigate, signed]);

  return (
    <S.Wrapper>
      <Helmet title={'SignIn | Ioasys Books'} />
      <S.Content>
        <CS.LogoHeader>
          <CS.Logo light />
          <CS.LogoTitle light>Books</CS.LogoTitle>
        </CS.LogoHeader>

        <C.SignInForm />
      </S.Content>
    </S.Wrapper>
  );
};

export default SignIn;

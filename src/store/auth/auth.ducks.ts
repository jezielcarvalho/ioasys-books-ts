import { createAction, createReducer } from '@reduxjs/toolkit';
import { SignInType, UserType } from 'interfaces/auth';

export type AuthState = {
  user: UserType;
  errors: Record<'message', string> | null;
  error: boolean;
  signed: boolean;
  loading: boolean;
  resetRequest: boolean;
};

const INITIAL_STATE: AuthState = {
  user: {} as UserType,
  errors: {
    message: '',
  },
  error: false,
  signed: false,
  loading: false,
  resetRequest: false,
};

export const login = createAction<SignInType, 'LOGIN'>('LOGIN');
export const setUser = createAction<SignInType, 'SET_USER'>('SET_USER');
export const loginLoading = createAction('LOGIN_LOADING');
export const loginError = createAction('LOGIN_ERROR');
export const loginSuccess = createAction('LOGIN_SUCCESS');
export const logout = createAction('LOGOUT');

export default createReducer(INITIAL_STATE, {
  [login.type]: (state, action) => ({ ...state, user: action.payload }),
  [setUser.type]: (state, action) => ({ ...state, user: action.payload }),
  [loginLoading.type]: (state) => ({
    ...state,
    errors: null,
    error: false,
    signed: false,
    loading: true,
  }),
  [loginError.type]: (state, action) => ({
    ...state,
    loading: false,
    error: true,
    errors: action.payload,
  }),
  [loginSuccess.type]: (state, action) => ({
    ...state,
    errors: null,
    error: false,
    signed: true,
    loading: false,
    user: action.payload,
  }),
  [logout.type]: () => ({
    user: {} as UserType,
    errors: null,
    error: false,
    signed: false,
    loading: false,
    resetRequest: false,
  }),
});

import { useCallback, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { ROUTES } from 'constants/urls';

/**
 * Book Modal Hook
 */
export default function useBookModal() {
  const [show, setShow] = useState(true);
  const navigate = useNavigate();

  // ease-out close modal
  const handleClose = useCallback(() => {
    // wait animation to
    setTimeout(() => {
      // redirect
      navigate(ROUTES.app.getLink('books'));
    }, 150);
    // trigger to animation close
    setShow(false);
  }, [navigate]);

  return { show, handleClose };
}

import {
  useState,
  useEffect,
  Dispatch,
  SetStateAction,
  useCallback,
} from 'react';
import Form from 'services/form';
import { SignInType } from 'interfaces/auth';
import { FormErrors } from 'interfaces/form';

type UseFormType = {
  handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  validated: boolean;
  values: SignInType;
  error: boolean;
  errors: FormErrors | null;
  showError: () => void;
  isSubmitting: boolean;
  setSubmitting: Dispatch<SetStateAction<boolean>>;
};

const useForm = (): UseFormType => {
  const [values, setValues] = useState({
    email: '',
    password: '',
  });
  const [error, setError] = useState(false);
  const [errors, setErrors] = useState<FormErrors | null>(null);
  const [isEmpty, setIsEmpty] = useState(true);
  const [isSubmitting, setSubmitting] = useState(false);

  useEffect(() => {
    setIsEmpty(!Object.values(values).some((value) => value !== ''));
  }, [values]);

  useEffect(() => {
    const { isValidated, errors } = Form.validate(values);
    setError(!isValidated && !isEmpty);
    setErrors(errors);
  }, [isEmpty, values]);

  const handleChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    setSubmitting(false);
    setValues((oldValues) => ({
      ...oldValues,
      [e.target.type]: e.target.value,
    }));
  }, []);

  const showError = useCallback(() => setError(true), []);

  return {
    handleChange,
    validated: !error && !isEmpty,
    values,
    error,
    errors,
    showError,
    isSubmitting,
    setSubmitting,
  };
};

export default useForm;

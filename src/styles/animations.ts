import { css } from 'styled-components';

export const fadeAnimations = css`
  animation: fade 0.5s ease-in-out;
  transition: transform 0.2s;

  @keyframes fade {
    0% {
      opacity: 0;
    }
  }
`;

export const bounceAnimation = css`
  animation: bounce 0.5s;

  @keyframes bounce {
    0% {
      transform: translateY(10px);
      opacity: 0;
    }
    100% {
      transform: translateY(0px);
      opacity: 1;
    }
  }
`;

export const scaleCard = css`
  transition: transform 0.2s;

  &:hover {
    animation: scale(1.04);
    transform: scale(1.04);
    -webkit-animation: scale(1.04);
    -webkit-transform: scale(1.04);
    box-shadow: 0px 5px 8px 0px rgb(9 24 39 / 5%);
  }

  @keyframes fade {
    0% {
      opacity: 0;
    }
  }
`;

export const scaleButton = css`
  transition: transform 0.2s;

  &:hover {
    animation: scale(1.1);
    transform: scale(1.1);
    -webkit-animation: scale(1.1);
    -webkit-transform: scale(1.1);
  }

  @keyframes fade {
    0% {
      opacity: 0;
    }
  }
`;

import { useAppSelector } from 'hooks';
import * as CS from 'components/structure';
import * as S from './TextInput.styles';

export type TextInputProps = {
  children?: React.ReactChild;
  type: string;
  label: string;
  value: string;
  onChange(e: React.ChangeEvent<HTMLInputElement>): void;
  onSubmit?: () => void;
  canSubmit?: boolean;
};

export const TextInput = ({
  type,
  label,
  value,
  onChange,
  onSubmit,
}: TextInputProps) => {
  const { loading } = useAppSelector((state) => state.auth);

  return (
    <S.Wrapper>
      <S.InputContainer>
        <S.Label>{label}</S.Label>
        <S.InputValue type={type} onChange={onChange} value={value} />
      </S.InputContainer>

      <S.ActionContainer>
        {onSubmit && (
          <S.Button onClick={onSubmit}>
            {loading ? <CS.Loading /> : 'Entrar'}
          </S.Button>
        )}
      </S.ActionContainer>
    </S.Wrapper>
  );
};

import styled from 'styled-components';
import { Column } from 'components/structure';

export const Wrapper = styled(Column)`
  justify-content: end;
`;

import React from 'react';
import { login } from 'store/auth/auth.ducks';
import { useAppDispatch, useAppSelector, useForm } from 'hooks';
import * as CF from 'components/form';
import * as CS from 'components/structure';
import { SignInType } from 'interfaces/auth';
import * as S from './SignInForm.styles';

export const SignInForm = () => {
  const {
    values,
    error,
    validated,
    showError,
    isSubmitting,
    setSubmitting,
    handleChange,
  } = useForm();
  const { signed, error: requestError } = useAppSelector((state) => state.auth);
  const dispatch = useAppDispatch();

  const signIn = (user: SignInType) => dispatch(login(user));

  const handleSignIn = () => {
    setSubmitting(true);
    if (validated && !signed) {
      signIn(values);
    } else {
      showError();
    }
  };

  return (
    <S.Wrapper>
      <CF.TextInput
        type="email"
        label={'Email'}
        value={values['email']}
        onChange={handleChange}
      />
      <CF.TextInput
        type="password"
        label={'Senha'}
        value={values['password']}
        onChange={handleChange}
        // this input can additionally submit the form
        onSubmit={handleSignIn}
      />

      {((isSubmitting && error) || requestError) && <CS.FormError />}
    </S.Wrapper>
  );
};

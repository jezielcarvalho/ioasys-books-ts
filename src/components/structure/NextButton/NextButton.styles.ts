import styled from 'styled-components';
import { scaleButton } from 'styles/animations';

export type WrapperProps = {
  disabled?: boolean;
  onClick?: () => void;
};

export const Wrapper = styled.div<WrapperProps>`
  cursor: pointer;

  svg {
    ${({ disabled }) => (disabled ? 'unset' : scaleButton)}
  }
`;

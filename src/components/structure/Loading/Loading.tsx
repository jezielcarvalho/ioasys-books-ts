import * as S from './Loading.styles';

export type LoadingProps = {
  children?: React.ReactChild;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const Loading = ({ children }: LoadingProps) => (
  <S.Wrapper>
    <S.Loading />
  </S.Wrapper>
);

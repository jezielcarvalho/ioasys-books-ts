import { Meta, Story } from '@storybook/react/types-6-0';
import { Loading, LoadingProps } from './Loading';

export default {
  title: 'structure/Loading',
  component: Loading,
} as Meta;

export const Default: Story<LoadingProps> = (args) => <Loading {...args} />;

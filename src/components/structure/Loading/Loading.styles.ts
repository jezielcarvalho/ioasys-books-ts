import styled, { css } from 'styled-components';

export const Wrapper = styled.div`
  ${() => css`
    display: flex;
    width: 85px;
    justify-content: center;
  `}
`;

export const Loading = styled.div`
  ${({ theme }) => css`
    border: 3px solid ${theme.colors.neutral.darkgrayopacity};
    border-left-color: ${theme.colors.secondary.main};
    border-radius: 50%;
    width: 16px;
    height: 16px;
    animation: spin 1s linear infinite;

    @keyframes spin {
      to {
        transform: rotate(360deg);
      }
    }
  `}
`;

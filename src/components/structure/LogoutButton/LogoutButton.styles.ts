import styled from 'styled-components';
import { scaleButton } from 'styles/animations';

export const Wrapper = styled.div`
  cursor: pointer;

  svg {
    ${scaleButton}
  }
`;

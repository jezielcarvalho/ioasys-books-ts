import styled, { css } from 'styled-components';
import { bounceAnimation } from 'styles/animations';

export const Wrapper = styled.div`
  position: relative;
  margin-top: 8px;

  ${bounceAnimation}
`;

export const ErrorMessage = styled.span`
  ${({ theme }) => css`
    position: absolute;
    top: 24px;
    left: 20px;
    color: ${theme.colors.neutral.white};
    font-weight: ${theme.typography.fontWeight.bold};
    font-size: ${theme.typography.sizes.xs};
  `}
`;
